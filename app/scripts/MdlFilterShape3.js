var MdlFilterShape = {
  shape:null,
  marker:null,
  bounds:null,
  json:null,
  radius:null,
  filterType:null,
  center:null,
  filterCircle:function (lat,lon) {
    var res = false;
    console.log("filtro circulo");
    var distance = MdlFilterShape.center.distanceTo([lat,lon]);
    res = distance < MdlFilterShape.radius;
    return res;
  },
  filterPolygon:function (lat,lon) {
    var res = false;
    var results = leafletPip.pointInLayer(L.latLng(lat,lon),   MdlFilterShape.json,true);
      console.log(results);

      console.log((results.length !== 0 ));

      res = (results.length !== 0 )
      return res;
  },
  calculateBounds:function(){
    if (this.shape) {
      var bounds =null;
      /*if(this.shape.getBounds){//circulo
        bounds = this.shape.getBounds();
      }else{//poligono*/
      if(!this.shape.getBounds){
        bounds = new google.maps.LatLngBounds();

        var points = this.shape.getPath().getArray();
        //console.info(points);
        var count = points.length;
        for (var i = 0; i < count; i++) {
          var punto = points[i];
          //console.info(punto);
          bounds.extend(punto);
        }
      }
      //console.info(bounds.toString());
      this.bounds = bounds;
    }

  },
  finishShape: function (polygon) {
    if (this.shape) {
      map.map.removeLayer(this.shape);
      this.shape=null;
    }
    polygon.on('edit', function() {
            console.log('Circle was edited!');
            MdlFilterShape.update(polygon);
        });
        

    polygon.on('centerchange', function() {
      console.log('the circle has moved.');
      MdlFilterShape.update(polygon);
    });

    polygon.on('radiuschange', function() {
      console.log('the radius has chahged to: ' + this._mRadius);
      MdlFilterShape.update(polygon);
    });
    
    this.update(polygon);


    this.shape = polygon;
    //this.calculateBounds();
  },
  update:function (polygon) {
    var geojson = polygon.toGeoJSON();
      console.info(geojson);
      
      var featuretype = geojson.geometry.type;
      if (featuretype === "Point")
      {
        this.filterType = this.filterCircle;
        this.radius = polygon.getRadius();
        this.center = polygon.getLatLng();
      } else{
        this.filterType = this.filterPolygon;
        var gjLayer = L.geoJson( geojson);
         this.json = gjLayer;
      }
  },
  createPolygon:function(center){
    //this.createMarker(center);
    console.info(center);

    var centLat = center.lat;
    var centLon = center.lng;

    
    
    var polygon = new L.Polygon([
      new L.LatLng(centLat + 0.05, centLon),
      new L.LatLng(centLat, centLon - 0.05),
      new L.LatLng(centLat - 0.05, centLon),
      new L.LatLng(centLat, centLon + 0.05),
      new L.LatLng(centLat + 0.05, centLon)
    ]);

    polygon.editing.enable();

    map.map.addLayer(polygon);


    

    this.finishShape(polygon);
  },
  createCircle:function(center,radio){
    var lat = center.lat,
        lon = center.lng;

    var circleLocation = new L.LatLng(lat, lon),
    circleOptions = {
        color: 'red', 
        fillColor: 'yellow', 
        fillOpacity: 0.7
    };
        var circle = new L.CircleEditor(circleLocation, radio, circleOptions);
    
    map.map.addLayer(circle);




    this.finishShape(circle);
  },
  filtro:function(lat,lon){
    var res = false;
    
    if (this.filterType) {

      res = this.filterType(lat,lon);
    } 


    return res;
  },
  limpiar:function(){
    console.info("this",this);
    if (this.shape) {
      map.map.removeLayer(this.shape);
    }
    this.shape = null;
    this.bounds = null;
  }

};

var rad = function(x) {
  return x * Math.PI / 180;
};

var getDistance = function(p1, lat, lon) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = rad(lat - p1.lat);
  var dLong = rad(lon - p1.lng);
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.lat())) * Math.cos(rad(lat)) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d; // returns the distance in meter
};
